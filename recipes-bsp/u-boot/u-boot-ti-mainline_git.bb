require u-boot-ti.inc

LIC_FILES_CHKSUM = "file://Licenses/README;md5=5a7450c57ffe5ae63fd732446b988025"

PR = "r0"

PV = "2020.10"
UBOOT_GIT_URI = "git://gitlab.denx.de/u-boot/u-boot.git"
UBOOT_GIT_PROTOCOL = "http"
SRCREV = "050acee119b3757fee3bd128f55d720fdd9bb890"
